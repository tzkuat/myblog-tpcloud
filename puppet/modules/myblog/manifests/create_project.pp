class myblog::create_project {
    # Create the Mezzanine project    
    exec { "init-mezzanine-project":
            command => "/usr/local/bin/mezzanine-project myblog $myblog::app_path",
            user => "mezzanine",
            creates => "$myblog::app_path/__init__.py",
            notify  => Exec["init-mezzanine-db"]
    }

    # Create the development SQLite database
    exec { "init-mezzanine-db":
            command => "/usr/bin/python3 manage.py createdb --noinput",
            user => "mezzanine",
            cwd => "$myblog::app_path",
            refreshonly => true
    }
}
