class myblog::requirements {
    $packages = ["python3-dev", "python3-pip", "libtiff5-dev", "libjpeg8-dev", "zlib1g-dev", "libfreetype6-dev"]
    package { $packages:
            ensure  => installed
    }
    $pip_packages = ["Mezzanine"]
    package { $pip_packages:
            ensure  => installed,
            provider => pip3,
            require => Package[$packages]
            }

            user { "mezzanine":
                    ensure  => present
            }

            file { "$myblog::app_path":
                    ensure  => "directory",
                    owner   => "mezzanine",
                    group   => "mezzanine"
            }
}
